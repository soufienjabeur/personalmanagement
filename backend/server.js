let express = require('express')
path = require('path')
mongoose = require('mongoose')
cors = require('cors')
badyParse = require('body-parser')
dbConfig = require('./database/db');

//Connecting with mongo db
mongoose.Promise = global.Promise;
mongoose.connect(dbConfig.db, { useNewUrlParser: true }).then(
  () => {
    console.log('DataBase successfully connected')
  },
  (error) => {
    console.log('DataBase could not connected ' + error)
  }
)

// Setting up code with express js
const personalRoute = require('../backend/routes/personal.route')
const bodyParser = require('body-parser')
const app = express();
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(cors);
app.use(express.static(path.join(__dirname, 'dist/PersonalManagement')));
app.use('/', express.static(path.join(__dirname, 'dist/PersonalManagement')))
app.use('/api', personalRoute);

//Create Port
const port= process.env.PORT||4000;
const server= app.listen(port, ()=>{ console.log('Connecting to port ' +port)})

// Find 404 error and hand over to error handler
app.use((req, res, next)=>{
  next(creatError(404));
});

// error handler
app.use(function (err, req, res, next) {
  console.error(err.message); // Log error message in our server's console
  if (!err.statusCode) err.statusCode = 500; // If err has no specified error code, set error code to 'Internal Server Error (500)'
  res.status(err.statusCode).send(err.message); // All HTTP requests must have a response, so let's send back an error with its status code and message
});
