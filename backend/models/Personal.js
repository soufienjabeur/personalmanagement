const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Definition of collection and schema

let Personal = new Schema({

  designation: {
    type: String
  },
  firstname: {
    type: String
  },
  lastname: {
    type: String
  },
  email: {
    type: String
  },
  city: {
    type: String
  },
  state: {
    type: String
  },
  postalCode: {
    type: Number
  },
  dateBirth: {
    type: Date
  },
  image: {
    type: String
  },
  cv: {
    type: String
  },
  message: {
    type: String
  }
}, {
  collection: 'personnals'
})

module.exports = mongoose.model('Personal', Personal);
