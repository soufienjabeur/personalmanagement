const express = require('express');
const app = express();
const personalRoute = express.Router();

// Personal model
let Personal = require('../models/Personal');

// Add Personal
personalRoute.route('/create').post((req, res, next) => {
  Personal.create(req.body, (error, data) => {
    if (error) {
      return next(error);
    }
    else {
      res.json(data);
    }
  })
});

// Get All Personals
personalRoute.route('/').get((req, res) => {
  Personal.find((error, data) => {
    if (error) {
      return next(error);
    }
    else {
      res.json(data);
    }
  });
});

// Get Single Personal
personalRoute.route('/read/:id').get((req, res) => {
  Personal.findById(req.params.id, (error, data) => {
    if (error) {
      return next(error);
    }
    else {
      res.json(data);
    }
  })
})

// Update Personal
personalRoute.route('/update/:id').put((req, res, next) => {
  Personal.findByIdAndUpdate(req.params.id, { $set: req.body }, (error, data) => {
    if (error) {
      return next(error);
      console.log(error);
    }
    else {
      res.json(data);
      console.log('Successfully updating Personal Profil ');
    }
  });
});

// Delete Personal

personalRoute.route('/delete/:id').delete((req, res, next) => {
  Personal.findByIdAndRemove(req.params.id, (error, data) => {
    if (error) {
      return next(error);
    }
    else {
      res.status(200).json({ msg: 200 });
    }
  });
});

module.exports = personalRoute;
